<?php

require 'src/conexao.php';
require 'src/functionsCliente.php';
include 'src/redireciona.php';


if($_SERVER['REQUEST_METHOD']==='POST'){
    
    $exibeCli=new function_clientes($mysql);
    $cliente_consulta=$exibeCli->exibeBusca($_POST['cnpjBusca']);

    // if(empty($cliente_consulta)){
    //     $exibeCli = new function_clientes($mysql);
    //     $cliente_consulta=$exibeCli->exibeCliente();
    // }
   

}else{
    $exibeCli = new function_clientes($mysql);
    $cliente_consulta=$exibeCli->exibeCliente();

}





?>

<!DOCTYPE html>

<html lang="pt-BR">
    <head>
        <meta charset="UTF-8">
        <title>Clientes</title>
        <link rel= "stylesheet" href="style.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    </head>
    <body>
        <div class="bt-container">
            <button class="bt"><a href="clientes.php">CLIENTES</a></button>
            <button class="bt"><a href="vendas.php">VENDAS</a></button>
        </div>

       
        
        <form method="POST" action="clientes.php">
            <div class="pesquisa">
                <p class="form">CNPJ:</p>
                <input type="text" id="cnpjBusca"  name="cnpjBusca" value="">
                <button type="submit" class="pesquisar">Pesquisar</button>
            </div>
        </form>
        
        
        <div class="btnadd">
            <button class="adicionar"><a href="#abrirModal">CADASTRAR</a></button>
        </div>
            
        
      
        <div id="abrirModal" class="modal">

            <a href="#fechar" title="Fechar" class="fechar">x</a>
            <div class="modal_cadastro">

                <form method="POST" action="/callFuncClientes/cadastraCliente.php" id="post">
                    <h2>Cadastrar Cliente</h2>
                    <p class="form">CNPJ cliente:</p>
                        <div id="buscar">
                            <input id="busca" type="button" value="Buscar">
                        </div>
                    <input type="text" id="cnpj" name="cnpj" value="">
                    <p class="form">Razão social:</p>
                    <input type="text" id="razao"  name="razao" value="">
                    <p class="form">Nome fantasia:</p>
                    <input type="text" id="fantasia" name="fantasia" value="">
                    <p class="form">Valor de faturamento:</p>
                    <input type="text" id="faturamento" name="faturamento" value="">
                    <p class="form">Endereço:</p>
                    <input type="text" id="endereco" name="endereco" value="">
                    <input type="submit" value="Cadastrar" >
                    
                </form>
                <script type="text/javascript">
                    
                    $("#busca").on("click", function(){
                        
                        const recebecnpj=($("#cnpj").val().replace(/[^\d]+/g,''));
                        const cnpj=recebecnpj;
                        
                        
                        if ( cnpj.length != 14){
                            
                            alert("Favor verificar o CNPJ digitado!");

                        }else{

                            const url='callFuncClientes/buscaCNPJ.php?cnpj='+cnpj;
                            
                            function preenche(data)
                            {
                                const razao = document.querySelector("#razao");
                                const fantasia = document.querySelector("#fantasia");
                                const faturamento = document.querySelector("#faturamento");
                                const endereco = document.querySelector("#endereco");
                                
                                razao.value = data.nome;
                                fantasia.value = data.fantasia;
                                faturamento.value = data.capital_social.toString().replace(".",",");
                                endereco.value = data.logradouro;
                            
                            }
                            fetch(url).then(function(response){
                                    if(!response.ok){
                                        alert("teste");
                                    
                                    }
                                    return response.json();    
                                
                                })
                                
                        
                                .then(function(data){
                                    console.log(data.status);
                                    if(data.status=="OK"){
                                        preenche(data);
                                        
                                    }else{
                                        alert('CNPJ inválido');
                                    }                                   
                                })
                        }
                        
                    })
                            
                       
                        
	            </script>
            </div>   
        </div>
        <?php foreach($cliente_consulta as $exibeCli): ?>
        <div id="abrirModalEditar?cnpj=<?php echo $exibeCli['cnpj']?>" class="modal">
        
            <a href="clientes.php" title="Fechar" class="fechar">x</a>
            <div class="modal_cadastro">              
                <form method="POST" action="/callFuncClientes/editaCliente.php">
                    <h2>Alterar dados</h2>
                    <p class="form">CNPJ cliente:</p>
                    <input type="text" name="cnpj" readonly="" value="<?php echo $exibeCli['cnpj']?>">
                    <p class="form">Razão social:</p>
                    <input type="text" name="razao" value="<?php echo $exibeCli['razaoSocial']?>">
                    <p class="form">Nome fantasia:</p>
                    <input type="text" name="fantasia" value="<?php echo $exibeCli['nomeFantasia']?>">
                    <p class="form">Valor de faturamento:</p>
                    <input type="text" name="faturamento" value="<?php echo $exibeCli['faturamento']?>">
                    <p  id="tabela" class="form">Endereço:</p>
                    <input type="text" name="endereco" value="<?php echo $exibeCli['endereco']?>">
                    <input type="submit" value="Confirmar Alterações">
                </form>
                
            </div>         
        </div>
        
        <div id="abrirModalExcluir?cnpj=<?php echo $exibeCli['cnpj']?>" class="modal">
            <a href="#fechar" title="Fechar" class="fechar">x</a>
            <div class="modal_cadastro"> 
                <form method="POST" action="/callFuncClientes/excluiCliente.php">
                    <input type="hidden" name="cnpj" value="<?php echo $exibeCli['cnpj'] ?>">
                    <p>Tem certeza que deseja excluir este cliente?</p>
                    <input type="submit" value="Excluir">
                </form>
            </div>        
        </div>
        <?php endforeach?>
        <table>
            <tr>
                <th>CNPJ</th>
                <th>RAZÃO SOCIAL</th>
                <th>NOME FANTASIA</th>
                <th>FATURAMENTO</th>
                <th>ENDEREÇO</th>  
            </tr>
            <?php foreach($cliente_consulta as $exibeCli): ?>
            <tr>
                <td><?php echo $exibeCli['cnpj'] ?></td>
                <td><?php echo ucwords(strtolower($exibeCli['razaoSocial'])) ?></td>
                <td><?php echo ucwords(strtolower($exibeCli['nomeFantasia'])) ?></td>
                <td><?php echo 'R$'.number_format($exibeCli['faturamento'],2,',','.') ?></td>
                <td><?php echo  ucwords(strtolower($exibeCli['endereco'])) ?></td>
                <td> <a id="altera" href="#abrirModalEditar?cnpj=<?php echo $exibeCli['cnpj']?>" >Editar</td>
                <td> <a id="altera" href="#abrirModalExcluir?cnpj=<?php echo $exibeCli['cnpj']?>">Excluir</td>
            </tr>
            <?php endforeach ?>
        </table>
            
    </body>
</html>