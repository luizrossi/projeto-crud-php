<!DOCTYPE html>
<?php

require 'src/conexao.php';
require 'src/functionsVenda.php';
include 'src/redireciona.php';

if($_SERVER['REQUEST_METHOD']==='POST'){
    $exibeVenda = new function_venda($mysql);
    $consulta_venda=$exibeVenda->exibeBusca($_POST['cnpjBusca']);
}else{
    $exibeVenda = new function_venda($mysql);
    $consulta_venda=$exibeVenda->exibeVendas();
}



?>

<html lang="pt-BR">
    <head>
        <meta charset="UTF-8">
        <title>Vendas</title>
        <link rel= "stylesheet" href="style.css">
    </head>
    <body>
        <div class="bt-container">
            <button class="bt"><a href="clientes.php">CLIENTES</a></button>
            <button class="bt"><a href="vendas.php">VENDAS</a></button>
        </div>
    
        <form method="POST" action="vendas.php">
            <div class="pesquisa">
                <p class="form">CNPJ:</p>
                <input type="text" id="cnpjBusca"  name="cnpjBusca" value="">
                <button type="submit" class="pesquisar">Pesquisar</button>
            </div>
        </form>
        
        
        <div class="btnadd">
            <button class="adicionar"><a href="#abrirModal">CADASTRAR</a></button>
        </div>
        
        <div id="abrirModal" class="modal">
            <a href="#fechar" title="Fechar" class="fechar">x</a>
            <div class="modal_cadastro">
                <form method="POST" action="/callFuncVendas/cadastraVenda.php">
                    <h2>Cadastrar Venda</h2>
                    <p class="form">CNPJ do cliente:</p>
                    <input type="text" name="cliente">
                    <p class="form">Valor da venda:</p>
                    <input type="text" name="valor">
                    <p class="form">Data da venda:</p>
                    <input type="date" name="data">
                    <input type="submit" value="Cadastrar">
                </form>   
            </div>
        </div>

        <?php foreach($consulta_venda as $exibeVenda):?>
        <div id="abrirModalExcluir?codvenda=<?php echo $exibeVenda['cod_venda']?>" class="modal">
            <a href="#fechar" title="Fechar" class="fechar">x</a>
            <div class="modal_cadastro"> 
                <p>Tem certeza que deseja excluir a venda: <?php echo $exibeVenda['cod_venda']?>?</p>
                <form method="POST" action="/callFuncVendas/excluiVenda.php">
                    <input type="hidden" name="cod_venda" value="<?php echo $exibeVenda['cod_venda'] ?>">
                    <input type="submit" value="Excluir">
                </form>
            </div>        
        </div>
        <?php endforeach?>

        <table>

            <tr>
                <th>CÓD.</th>
                <th>DATA </th>
                <th>CNPJ</th>
                <th>CLIENTE</th>
                <th>VALOR</th>
                 
            </tr>

            <?php foreach($consulta_venda as $exibeVenda):?>
            <tr>
                <td><?php echo $exibeVenda['cod_venda']?></td>
                <td><?php echo $data = implode("/",array_reverse(explode("-",$exibeVenda['data_venda'])));?></td>
                <td><?php echo $exibeVenda['cnpj_cliente']?></td>
                <td><?php echo ucwords(strtolower($exibeVenda['razaoSocial']))?></td>
                <td><?php echo 'R$'.number_format($exibeVenda['valor_venda'],2,',','.')?></td>
                <td><a id="altera" href="#abrirModalExcluir?codvenda=<?php echo $exibeVenda['cod_venda']?>">Excluir</a></td>
            </tr>
            <?php endforeach?>

        </table>
            
    </body>
</html>