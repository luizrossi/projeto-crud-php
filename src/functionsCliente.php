<?php


class function_clientes
{
    
    private $mysql;
    
    public function __construct(mysqli $mysql)
    {
        $this->mysql = $mysql;
    }

    public function cadastraCliente(string $cnpj, string $razao, string $fantasia, string $faturamento, string $endereco)
    {
        
            
            try{
                $insere_cliente = $this->mysql->prepare('INSERT INTO cliente (cnpj, razaoSocial, nomeFantasia, faturamento, endereco)
                VALUES (?,?,?,?,?)');
                $insere_cliente->bind_param('sssss', $cnpj, $razao, $fantasia, $faturamento, $endereco);
                $insere_cliente->execute();
                if($insere_cliente->error){
                    throw new Exception('Não é possível cadastrar dois clientes com o mesmo CNPJ!');
                }else{
                    redireciona('../clientes.php');
                }
            }catch(Exception $erro){
                echo "Exceção capturada: {$erro->getMessage()}";
                
            }

    }

    public function exibeCliente()
    {
        $consulta_clientes= $this->mysql->query('SELECT cnpj, razaoSocial, nomeFantasia, faturamento, endereco
        FROM cliente');
        $clientes_consulta= $consulta_clientes->fetch_all(MYSQLI_ASSOC);

        return $clientes_consulta;
    }

    public function exibeBusca(string $cnpj)
    {   

        if(!empty($cnpj)){
            
            $consulta_cliente= $this->mysql->query('SELECT cnpj, razaoSocial, nomeFantasia, faturamento, endereco
            FROM cliente WHERE cnpj='.$cnpj);
            $cliente_consulta= $consulta_cliente->fetch_all(MYSQLI_ASSOC);
            return $cliente_consulta;
            
        }else{
            redireciona('../clientes.php');
        }
        
    }


    public function editarCliente(string $cnpj, string $razao, string $fantasia, string $faturamento, string $endereco)
    {
   
        $edita_cliente = $this->mysql->prepare('UPDATE cliente SET razaoSocial=?, nomeFantasia=?, faturamento=?, endereco=?
        WHERE cnpj=?');
        $edita_cliente->bind_param('sssss', $razao, $fantasia, $faturamento, $endereco, $cnpj);
        $edita_cliente->execute();
        

    }

   public function excluirCliente(string $cnpj)
   {
       
        try{
            $exclui_cliente=$this->mysql->prepare('DELETE FROM cliente WHERE cnpj = ?');
            $exclui_cliente->bind_param('s', $cnpj);
            $exclui_cliente->execute();
            if($exclui_cliente->error){
                throw new Exception('Não é possível excluir o cliente pois o mesmo tem vendas em seu nome!');
            }else{
                redireciona('../clientes.php');
            }
        }catch(Exception $erro){
            echo "Exceção capturada: {$erro->getMessage()}";
        }
   }


}

?>
