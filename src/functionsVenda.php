<?php 

class function_venda{

    private $mysql;
    
    public function __construct(mysqli $mysql)
    {
        $this->mysql = $mysql;
    }

    public function cadastraVenda(string $cnpj, string $data, string $valor)
    {
        try{
            $data_f=date('Y-m-d', strtotime($data));
            $insere_venda=$this->mysql->prepare('INSERT INTO venda (cnpj_cliente, data_venda, valor_venda) VALUES (?,?,?)');
            $insere_venda->bind_param('ssd', $cnpj, $data_f, $valor);
            $insere_venda->execute();
            if($insere_venda->error){
                throw new Exception('Cliente não cadastrado!');
            }else{
                // return $insere_venda;
                redireciona('../vendas.php');
            }
        
        }catch(Exception $erro){
            echo "Exceção capturada: {$erro->getMessage()}";
        }
       
        

    }
    public function exibeVendas()
    {

        $consulta_vendas=$this->mysql->query('SELECT v.cod_venda, v.valor_venda, v.data_venda, v.cnpj_cliente, c.cnpj, c.razaoSocial 
        FROM venda v
        INNER JOIN cliente c
            ON v.cnpj_cliente = c.cnpj');
        $c_vendas= $consulta_vendas->fetch_all(MYSQLI_ASSOC);
    
        return $c_vendas;
    }

    public function exibeBusca(string $cnpj)
    {
        if(!empty($cnpj)){
            $consulta_vendas=$this->mysql->query('SELECT v.cod_venda, v.valor_venda, v.data_venda, v.cnpj_cliente, c.cnpj, c.razaoSocial 
            FROM venda v
            INNER JOIN cliente c
                ON v.cnpj_cliente = c.cnpj WHERE v.cnpj_cliente='.$cnpj);
            $c_vendas= $consulta_vendas->fetch_all(MYSQLI_ASSOC);

            return $c_vendas;
        }else{
            redireciona('../vendas.php');
        }
    }

    public function excluiVendas(string $cod_venda)
    {
        $excluir_venda=$this->mysql->prepare('DELETE FROM venda WHERE cod_venda = ?');
        $excluir_venda->bind_param('s', $cod_venda);
        $excluir_venda->execute();
    }
}



?>






